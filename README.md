# Terminale NSI

Dépôt pour les cours et activités de Terminale NSI au lycée Emmanuel Mounier à Angers.

Ce dépôt sera alimenté au fur et à mesure.

Le site utilisé par les élèves est : https://info-mounier.fr/terminale_nsi/

Sauf mention contraire, tous les documents présents sur ce dépôt sont sous licence CC-BY-NC-SA

![Licence Creative Commons](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)