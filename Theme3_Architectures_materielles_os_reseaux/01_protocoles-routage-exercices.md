Protocoles de routage - EXERCICES
=================================

# Exercice 1 - Le protocole RIP en mode débranché

Par groupe de 2 ou 3 élèves, vous allez incarner un routeur et disposerez donc d'une **table de routage** qu'il faudra mettre à jour selon le protocole RIP.

> On supposera ici que tous les routeurs "arrivent" dans le réseau au même moment, et ont tous des tables de routage vides. Cette situation n'est pas vraiment réaliste puisqu'en général, les nouveaux routeurs arrivent dans un réseau existant, tous les autres ayant déjà des tables de routages complètes.

**Initialisation : tour 0**

Au démarrage, chaque routeur ne connaît que ses voisins et le nombre de saut pour les atteindre est égal à 1.

**Tours suivants**

Chaque routeur va prendre en photo la table de routage de **ses voisins** et met à jour la sienne en conséquence. On rappelle que la mise à jour des tables dans le protocole RIP se fait de la façon suivante :

* si une route reçue comprend un chemin plus court que celui qu'il connait : il modifie sa table avec ce plus court chemin
* si une route n'est pas connue, il l'ajoute à sa table en ajoutant 1 au nombre de sauts du voisin qui lui a transmis la route
* sinon, il n'y a pas de changement

> Pour simplifier, on ne demandera que d'indiquer les noms des routeurs voisins, en réalité il faudrait indiquer leur adresse IP dans le sous-réseau commun ! De même, pour le moment, on se préoccupe pas des interfaces (elles ne sont pas données).

**Après convergence des tables, dessinez une représentation possible du réseau tout entier.**

# Exercice 2 - Tables plus complètes

Voici le schéma du réseau de l'exercice 1. 

<img class="centre image-responsive" alt="schéma complet du réseau" src="data/reseau_parts.svg">

Utilisez ce schéma par compléter la table de chaque routeur obtenue dans l'exercice 1, en indiquant les adresses IP des passerelles et en ajoutant la colonne *Interface*.

# Exercice 3 - Protocole RIP

Considérons le réseau suivant dans lequel le protocole de routage est **RIP**.

<img class="centre image-responsive" alt="schéma d'un réseau" src="data/reseau_ex3_initial.png">

**Q1** : On suppose que les tables ont eu le temps de converger et qu'il n'y a pas de pannes. Donnez le contenu de la table de routage des routeurs R1 et R4.

**Q2** : On rajoute un routeur **R5** dans le réseau comme ci-dessous. 

<img class="centre image-responsive" alt="schéma d'un réseau" src="data/reseau_ex3.png">

- R5 envoie alors une *requête RIP* à ses voisins : R1 et R4.
- R1 et R4 lui envoient une réponse contenant leurs tables de routage

Actualisez la table de routage de R5 en fonction des réponses de R1 et R4.

**Q3** : R5 envoie ensuite sa table de routage à ses voisins pour qu'ils actualisent les leurs. Donnez les tables actualisées des routeurs R1 et R4.

**Q4** : Les routeurs R1 et R4 ont modifié leur tables de routage et doivent propager l'information. À qui envoient-ils leur nouvelle table ? Les autres routeurs doivent-ils actualiser leurs tables ? Expliquer.

# Exercice 4 - Protocole OSPF

Voici un réseau contenant 7 routeurs R1, R2, ..., R7 et dans lequel le protocole de routage utilisé est OSPF.

<img class="centre image-responsive" alt="graphe" src="data/ex4.png">

1. Donnez le graphe correspondant pondéré par les coûts de chaque liaison.
2. Après différents échanges, les routeurs ont la connaissance complète de la topologie du réseau. Donnez cette topologie sous forme d'un tableau.

| Lien | Coût |
| --- | --- |
| R1-R2 | ... |
| ... | ... |

3. Recopier et compléter la table de routage du routeur R1 (en déterminant *de tête* les meilleurs chemins). Pour simplifier, vous indiquerez les noms des routeurs dans les deux premières colonnes (ce sont normalement des @IP). 

| Destination | En passant par | Coût |
| --- | --- | --- |
| R2 | | ... |
| ... | | |

# Exercice 5 - Plus court chemin dans un graphe (Dijkstra)

**Question 1**

1. Appliquez l'algorithme de Dijsktra pour trouver le meilleur chemin entre les routeurs R1 et R7 du réseau de l'exercice précédent.
2. Quel serait le meilleur chemin si le protocole utilisé est RIP ?

**Question 2**

Appliquez l'algorithme de Dijkstra pour trouver le chemin le plus court pour aller de E à F.

<img class="centre image-responsive" alt="graphe" src="data/ex5.png">

**Question 3**

Appliquez l'algorithme de Dijkstra pour trouver le chemin le plus court pour aller de A à H.

<img class="centre image-responsive" alt="graphe" src="data/ex5_2.png">

> Correction en vidéo : [https://www.youtube.com/watch?v=rI-Rc7eF4iw&ab_channel=glassus](https://www.youtube.com/watch?v=rI-Rc7eF4iw&ab_channel=glassus)

# Exercice 6

Le réseau schématisé ci-dessous est constitué de 4 réseaux locaux (switch+ordinateurs) : R1, R2, R3 et R4, et de 4 routeurs A, B, C et D.

<img class="centre image-responsive" alt="graphe" src="data/ex6.png">

On donne les débits (bandes passantes) de la connexion entre chaque routeur :
A-B : 100 Mbps
A-D : 1000 Mbps
B-C : 10 Mbps
C-D : 10 Mbps
On propose ci-dessous une table de routage pour le routeur A :

<img class="centre image-responsive" alt="graphe" src="data/ex6_2.png">

**1.** Complétez la colonne “métrique RIP” du tableau ci-dessus.

**2.** Complétez la colonne “métrique OSPF” du tableau ci-dessus. 

**3.** Un ordinateur appartenant au réseau local R1 envoie un paquet de données à un ordinateur appartenant au réseau local R2.
    
- Donnez le chemin suivi par ce paquet de données si le routeur A utilise le protocole de routage RIP. Justifiez votre réponse.
- Donnez le chemin suivi par ce paquet de données si le routeur A utilise le protocole de routage OSPF. Justifiez votre réponse.
    
**4.** Le routeur D est en panne. Un ordinateur appartenant au réseau local R1 envoie un paquet de données à un ordinateur appartenant au réseau local R3. Donnez le chemin suivi par ce paquet de données si le routeur A utilise le protocole de routage OSPF. Justifiez votre réponse.

**5.** La liaison entre 2 routeurs a un coût de 0,2, calculez le débit de cette liaison en bps puis en Mbps.

# Exercice 7 - Sujet 0 NSI

> Cet exercice est tiré du sujet 0 du bac NSI.

On considère un réseau composé de plusieurs routeurs reliés de la façon suivante :

<img class="centre image-responsive" alt="schéma du réseau" src="data/ex7_1.png">

## Le protocole RIP

Le protocole RIP permet de construire les tables de routage des différents routeurs, en indiquant pour chaque
routeur la distance, en nombre de sauts, qui le sépare d’un autre routeur. Pour le réseau ci-dessus, on dispose
des tables de routage suivantes :

<img class="centre image-responsive" alt="schéma du réseau" src="data/ex7_tables.png">

**Question 1**

1) Le routeur A doit transmettre un message au routeur G, en effectuant un nombre minimal de
sauts. Déterminer le trajet parcouru.

2) Déterminer une table de routage possible pour le routeur G obtenu à l’aide du protocole RIP.

**Question 2**

Le routeur C tombe en panne. Reconstruire la table de routage du routeur A en suivant le
protocole RIP.

## Le protocole OSPF

Contrairement au protocole RIP, l’objectif n’est plus de minimiser le nombre de routeurs traversés par un
paquet. La notion de distance utilisée dans le protocole OSPF est uniquement liée aux coûts des liaisons.
L’objectif est alors de minimiser la somme des coûts des liaisons traversées.

On a rajouté sur le graphe représentant le réseau précédent les différents débits des liaisons.
On rappelle que 1 Gb/s = 1 000 Mb/s = 10 9 bits/s.

<img class="centre image-responsive" alt="schéma du réseau avec débits" src="data/ex7_2.png">

**Question 3**

1) Vérifier que le coût de la liaison entre les routeurs A et B est 0,01.

2) La liaison entre le routeur B et D a un coût de 5. Quel est le débit de cette liaison ?

**Question 4** 

Le routeur A doit transmettre un message au routeur G, en empruntant le chemin dont la somme
des coûts sera la plus petite possible. Déterminer le chemin parcouru. On indiquera le raisonnement
utilisé.

# Exercice 8 

> Cet exercice est tiré du sujet NSI Métropole 2021 J2 (épreuve annulée). Le sujet a été modifié pour corriger les erreurs dans les adresses passerelles.

<img class="centre image-responsive" alt="schéma du réseau avec débits" src="data/ex8_fig1.png">
<p class="legende" style="text-align:center;"><strong>Figure 1 : Réseau d'entreprise</strong></p>

La figure 1 ci-dessus représente le schéma d’un réseau d’entreprise. Il y figure deux réseaux locaux L1 et L2. Ces deux réseaux locaux sont interconnectés par les routeurs R2, R3, R4 et R5. Le réseau local L1 est constitué des PC portables P1 et P2 connectés à la passerelle R1 par le switch Sw1. Les serveurs S1 et S2 sont connectés à la passerelle R6 par le switch Sw2.

 Le tableau 1 suivant indique les adresses IPv4 des machines constituants le réseau de l’entreprise.

<img class="centre image-responsive" alt="tableau des adresses IP" src="data/ex8_tab1.png">
<p class="legende" style="text-align:center;"><strong>Tableau 1 : adresses IPv4 des machines</strong></p>

**Rappels et notations**

Rappelons qu’une adresse IP est composée de 4 octets, soit 32 bits. Elle est notée
X1.X2.X3.X4, où X1, X2, X3 et X4 sont les valeurs des 4 octets. Dans le tableau 1, les valeurs des 4 octets ont été converties en notation décimale.

La notation X1.X2.X3.X4/n signifie que les n premiers bits de poids forts de l’adresse IP représentent la partie « réseau », les bits suivants de poids faibles représentent la partie « machine ».

Toutes les adresses des machines connectées à un réseau local ont la même partie réseau.
L’adresse IP dont tous les bits de la partie « machine » sont à 0 est appelée « adresse du réseau ».
L’adresse IP dont tous les bits de la partie « machine » sont à 1 est appelée « adresse de diffusion ».

**1.a.** Quelles sont les adresses des réseaux locaux L1 et L2 ?

**1.b.** Donner la plus petite et la plus grande adresse IP valides pouvant être attribuées à un ordinateur portable ou un serveur sur chacun des réseaux L1 et L2 sachant que l’adresse du réseau et l’adresse de diffusion ne peuvent pas être attribuées à une machine.

**1.c.** Combien de machines peut-on connecter au maximum à chacun des réseaux locaux L1 et L2 ? On donne ci-dessous les valeurs de quelques puissances de 2.

| $$2^6$$ | $2^7$ | $2^8$ | $2^9$ | $2^{10}$ | $2^{11}$ | $2^{12}$ | $2^{13}$ | $2^{14}$ | $2^{15}$ | $2^{16}$ | $2^{17}$ |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| 64 | 128 | 256 | 512 | 1024 | 2048 | 4096 | 8192 | 16384 | 32768 | 65536 | 131072 |

**2.a.** Expliquer l’utilité d’avoir plusieurs chemins possibles reliant les réseaux L1 et L2.

**2.b.** Quel est le chemin le plus court en nombre de sauts pour relier R1 et R6 ? Donner le nombre de sauts de ce chemin et préciser les routeurs utilisés.

**2.c.** La bande passante d’une liaison Ether (quantité d’information qui peut être transmise en bits/s) est de $10^7$ bits/s et celle d’une liaison FastEther est de $10^8$ bits/s. Le coût d’une liaison est défini par $\dfrac{10^8}{d}$ , où $d$ est sa bande passante en bits/s.

| Liaison | R1-R2 | R2-R5 | R5-R6 | R2-R3 | R3-R4 | R4-R5 | R3-R5 |
| --- | --- | --- |  --- | --- | --- |  --- | --- |
| Type | Ether | Ether | Ether | FastEther | FastEther | FastEther | Ether |

<p class="legende" style="text-align:center;"><strong>Tableau 2 : type des liaisons entre les routeurs</strong></p>

Quel est le chemin reliant R1 et R6 qui a le plus petit coût ? Donner le coût de ce chemin et préciser les routeurs utilisés.

**3.** Dans l’annexe A figurent les tables de routages des routeurs R1, R2, R5 et R6 au démarrage du réseau. Indiquer sur votre copie ce qui doit figurer dans les lignes laissées vides des tables de routage des routeurs R5 et R6 pour que les échanges entre les ordinateurs des réseaux L1 et L2 se fassent en empruntant le chemin le plus court en nombre de sauts.

<img class="centre image-responsive" alt="tables de routage" src="data/ex8_annexe.png">

# Exercice 9

> Exercice 5 du sujet [La Réunion J1 2022](https://pixees.fr/informatiquelycee/term/suj_bac/2022/sujet_10.pdf).

*Cet exercice porte sur : transmission de données dans un réseau, architecture d’un réseau, protocoles de routage, langages et programmation.*

Pour une "LAN PARTY", les organisateurs gèrent deux réseaux différents non liés physiquement suivant le schéma suivant :

**Réseau 1** : réseau contenant le commutateur 1 (switch1) ;  
**Réseau 2** : réseau contenant le commutateur 2 (switch2). 

<img class="centre image-responsive" alt="schéma de la LAN" src="data/ex9_lan.png">

Dans cet exercice, on exploitera la notation CIDR pour l'adressage des deux réseaux.

En notation CIDR, l'adresse IP d'une machine est composée d’une adresse IPv4 et d’une indication sur le masque de sous réseau. Par exemple : 172.16.1.10 / 16 signifie :
- Adresse IP décimale : 172.16.1.10
- Masque de sous-réseau en notation CIDR : 16

La notation CIDR /16 signifie que le masque de sous-réseau a les 16 bits de poids fort de son adresse IP à la valeur 1. C’est-à-dire, pour notre exemple: 11111111.11111111.00000000.00000000.

Le PC3 du réseau 1 a pour adresse IPv4 172.150.4.30/24

**1.a.** Combien d'octets sont nécessaires pour constituer une adresse IPv4 ?  
**1.b.** Quelle est la notation décimale du masque de sous-réseau du PC3 du réseau 1 ?

**2.** Pour déterminer l'adresse IP du réseau, recopier le tableau ci-dessous et compléter les cases vides, en suivant l'ordre des instructions suivantes : 

- compléter la ligne 2 : conversion de l'adresse IP décimale en adresse IP binaire sur 4 octets ;
- ligne 3 : compléter le masque de sous réseau en notation binaire ;
- ligne 4 : compléter l'adresse du réseau en notation binaire suite à un ET(&) logique entre chaque bit de la ligne 2 et la ligne 3 ;
- ligne 5 : compléter l'adresse IP décimale du réseau 1.

<img class="centre image-responsive" alt="tableau à compléter" src="data/ex9_tableau.png">

**3.a.** Parmi les propositions ci-dessous, déterminer, en justifiant, celle(s) qui pourrai(en)t être utilisée(s) pour associer un 4ème PC client au réseau 1:

1. 172.154.4.30
2. 172.150.4.10
3. 172.150.10.257
4. 172.150.4.11
5. 172.150.4.0
6. 172.150.4.200

**3.b.** Quelle commande permettrait de connaître son adresse IP ? 

Les organisateurs décident de faire une partie de jeu vidéo en connectant entre elles les machines des deux réseaux. Toutes les machines doivent être capables de communiquer entre elles. 

**4.** On décide de connecter directement le switch 1 avec le switch 2 pour réaliser cette nouvelle configuration du réseau. Expliquer pourquoi cette solution n’est pas satisfaisante ? Proposer une alternative ?

**5.** Dans le cadre d'une future "LAN PARTY", l'organisateur veut gérer la liste des IPv4 pour éviter que deux machines aient la même adresse. Il décide de commencer son étude en créant une fonction Python adresse.

Une liste de listes sera utilisée pour stocker les adresses IP des machines du réseau.

Par exemple :

```python
liste_IP = [[192, 168, 10, 1], [192, 168, 10, 25], [192, 168, 10, 13]]
```

La fonction `adresse` prend en paramètres l'adresse IP (sous la forme d’une liste) que l'on souhaite tester, une liste de listes (comme `liste_IP`) et :

- si l'adresse IP testée ne figure pas dans la liste, cette fonction l'ajoute à la liste des adresses IP du réseau et affiche le message `"pas trouvée, ajoutée"` ;
- si l'adresse IP testée figure dans la liste, cette fonction se contente d’afficher le message `"trouvée"`.

Exemples :

```python
>>> liste_IP = [[192, 168, 10, 1], [192, 168, 10, 25], [192, 168, 10, 13]]
>>> adresse([192, 168, 10, 3], liste_IP)
pas trouvée, ajoutée
>>> liste_IP
[[192, 168, 10, 1], [192, 168, 10, 25], [192, 168, 10, 13], [192, 168, 10, 3]]
>>> adresse([192, 168, 10, 25], liste_IP)
trouvée
>>> liste_IP
[[192, 168, 10, 1], [192, 168, 10, 25], [192, 168, 10, 13], [192, 168, 10, 3]]
```

Écrire en langage Python la fonction `adresse`. 

<br>

---

**Ressources**

- Exercices 1, 2 et 5 : d'après un [TP](https://github.com/glassus/terminale_nsi/tree/main/docs/T5_Architecture_materielle/5.3_Protocoles_de_routage/TP_protocole_RIP) et un [cours](https://github.com/glassus/terminale_nsi/blob/main/docs/T5_Architecture_materielle/5.3_Protocoles_de_routage/cours.md) de Gilles Lassus. 
- Exercice 3 : d'après un [cours sur les réseaux](http://sco.ljbrel93.ac-creteil.fr/eleyaouanc/1NSI/DOCUMENTS/reseau[CAMILLE_COTI].pdf) du DIU EIL Bloc 3 de Camille Coti (page 131/193, slide 86/136)
- Exercice 4 : d'après le [cours sur le protocole OSPF](https://www.lecluse.fr/nsi/NSI_T/archi/routage/OSPF/#exemple) d'Olivier Lécluse.
- Exercice 6 : d'après [un exercice](https://pixees.fr/informatiquelycee/term/c11e.html) proposé par David Roche.
- Exercices 7, 8 et 9 : extraits de sujets de bac.

---
Germain BECKER, Lycée Mounier, ANGERS

Ressource éducative libre distribuée sous [Licence Creative Commons Attribution - Partage dans les Mêmes Conditions 4.0 International](http://creativecommons.org/licenses/by-sa/4.0/) 

![Licence Creative Commons](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)
