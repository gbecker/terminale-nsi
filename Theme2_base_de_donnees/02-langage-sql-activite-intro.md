Langage SQL - Activité d'introduction
=====================================

# Introduction

Dans cette activité, vous allez découvrir comment effectuer des requêtes sur une table d'une base de données, en utilisant le langage SQL (pour *Structured Query Language*). 

Pour cela, nous allons travailler avec une table déjà étudiée l'année dernière en classe de Première, ce sera l'occasion de faire le lien avec ce qui a été vu l'an dernier.

Voici un extrait de la table utilisée l'année dernière :

| **prénom** | **jour** | **mois** | **année** | **sexe** | **classe** | **projet** |
| --- | --- | --- | --- | --- | --- | --- |
| Sylvain | 19 |2 | 2007 | G | 1G1 | manger des frites tous les jours à la cantine |
| Romain | 22 | 9 | 2006 | G | 1G4 | être heureux |
| Kenzo | 23 | 9 | 2006 | G | 1G3 | gagner au loto |
| Marin | 28 | 2 | 2006 | G | 1G1 | devenir quelqu’un de célèbre |
| Alexis | 27 | 7 | 2006 | G | 1G4 | avoir une bonne note au prochain devoir |
| Dario | 24 | 5 | 2006 | G | 1G3 | devenir astronaute |
| Adrien | 14 | 11 | 2006 | G | 1G3 | marcher sur la lune |
| Yanis | 2 | 6 | 2006 | G | 1G3 | dormir plus longtemps le matin |
| Sara | 14 | 1 | 2006 | F | 1G3 | aider les autres |
| Augustin | 24 | 10 | 2006 | G | 1G3 | apprendre à piloter une Formule 1 |
| ... | ... | ... | ... | ... | ... | ... |

Cette dernière était stockée dans un fichier `eleves.csv` que l'on pouvait importer en Python pour mémoriser son contenu dans un tableau de dictionnaires `eleves` :


```python
import csv
fichier = open('eleves.csv', 'r', encoding = 'UTF-8')  # ouverture du fichier
# Lecture de son contenu avec la méthode DictReader qui 
# renvoie une valeur spéciale représentant le fichier CSV
table = csv.DictReader(fichier, delimiter=',')  # caractère de séparation : la virgule
eleves = [dict(ligne) for ligne in table]  # chaque élément est un dictionnaire
fichier.close()  # fermeture du fichier
```

```python
>>> eleves
[{'prénom': 'Sylvain', 'jour': '19', 'mois': '2', 'année': '2007', 'sexe': 'G', 'classe': '1G1', 'projet': 'manger des frites tous les jours à la cantine'}, 
 {'prénom': 'Romain', 'jour': '22', 'mois': '9', 'année': '2006', 'sexe': 'G', 'classe': '1G4', 'projet': 'être heureux'}, 
 {'prénom': 'Kenzo', 'jour': '23', 'mois': '9', 'année': '2006', 'sexe': 'G', 'classe': '1G3', 'projet': 'gagner au loto'}, 
 ...
 {'prénom': 'Alexandre', 'jour': '27', 'mois': '5', 'année': '2006', 'sexe': 'G', 'classe': '1G3', 'projet': 'aller à Poudlard'}]
```

Par défaut, toutes les valeurs sont mémorisées comme des chaînes de caractères, y compris le jour, le mois et l'année. On peut effectuer un prétraitement pour convertir en type `int` chaque valeur associée aux clés `'jour'`, `'mois'` et `'annee'` : 


```python
eleves = [
    {'prénom': e['prénom'],
     'jour': int(e['jour']),
     'mois': int(e['mois']),
     'année': int(e['année']),
     'sexe': e['sexe'],
     'classe': e['classe'],
     'projet': e['projet']} for e in eleves
]
```

```python
>>> eleves
[{'prénom': 'Sylvain', 'jour': 19, 'mois': 2, 'année': 2007, 'sexe': 'G', 'classe': '1G1', 'projet': 'manger des frites tous les jours à la cantine'}, 
 {'prénom': 'Romain', 'jour': 22, 'mois': 9, 'année': 2006, 'sexe': 'G', 'classe': '1G4', 'projet': 'être heureux'}, 
 {'prénom': 'Kenzo', 'jour': 23, 'mois': 9, 'année': 2006, 'sexe': 'G', 'classe': '1G3', 'projet': 'gagner au loto'}, 
 ...
 {'prénom': 'Alexandre', 'jour': 27, 'mois': 5, 'année': 2006, 'sexe': 'G', 'classe': '1G3', 'projet': 'aller à Poudlard'}]
```

On a créé une base de données `eleves.db` qui ne contient que la *relation* `Eleve` suivante :

<pre style="padding-bottom:10px;">
<code><em>Eleve</em>(
    <span style="padding-bottom:3px; border-bottom: 1px solid black;"><em>id_eleve</em></span> INT, 
    <em>prenom</em> TEXT, 
    <em>jour</em> INT, 
    <em>mois</em> INT, 
    <em>annee</em> INT, 
    <em>sexe</em> TEXT, 
    <em>classe</em> TEXT, 
    <em>projet</em> TEXT
)</code>
</pre>

> On a ajouté un attribut `id_eleve`, pourquoi selon vous ?

**Remarque** : Le fichier `eleves.db` que nous utiliserons est accessible dans l'archive de ce chapitre.

# Utilisation d'un SGBD

Seul un logiciel de type SGBD (Système de Gestion de Bases de Données) peut manipuler les données présentes dans une base de données. 

Vous pouvez utiliser :

* **DB Browser for SQLite** qui est installé sur vos ordinateurs :
    * (ce logiciel est téléchargeable à l'adresse : [https://sqlitebrowser.org/](https://sqlitebrowser.org/))
    * Lancez le logiciel
    * Ouvrez le fichier `eleves.db` en le sélectionnant après avoir cliqué sur "Ouvrir une base de données"

* **sqliteonline** accessible en ligne à l'adresse [https://sqliteonline.com/](https://sqliteonline.com/).
    * Rendez-vous à l'adresse [https://sqliteonline.com/](https://sqliteonline.com/)
    * Cliquez sur _File_ puis sur _Open DB_ et sélectionnez le fichier `eleves.db`

# Première requête SQL

Commençons par une requête permettant d’afficher tout le contenu de la table `Eleve`.

```sql
SELECT * FROM Eleve;
```

**Analyse** :

* En SQL, chaque requête contient au moins les clauses `SELECT` et `FROM` et **se termine par un point-virgule**.
* Le mot-clé `SELECT` demande au SGBD d’afficher ce que contient une table.
* Après `SELECT`, il faut indiquer quels champs de la table, le SGBD doit récupérer dans celle-ci. Ici le caractère « `*` » indique qu’il faut récupérer **tous** les champs de la table.
* Après le `FROM` (de l’anglais « de ») on indique la table dans laquelle on veut récupérer des informations (ici `Eleve`)
* Bilan : cette requête se traduit par « prend tout ce qu’il y a dans la table `Eleve`, sous-entendu prend tous les champs de cette table.


**Q1** : Écrivez cette requête dans le SGBD puis exécutez-la. Vous devriez voir apparaître la table `Eleve` complète comme ci-dessous.

| id_eleve | prenom | jour | mois | annee | sexe | classe | projet |
|---:|:---|---:|---:|---:|:---|:---|:---|
| 1 | Sylvain | 19 | 2 | 2007 | G | 1G1 | manger des frites tous les jours à la cantine |
| 2 | Romain | 22 | 9 | 2006 | G | 1G4 | être heureux |
| 3 | Kenzo | 23 | 9 | 2006 | G | 1G3 | gagner au loto |
| 4 | Marin | 28 | 2 | 2006 | G | 1G1 | devenir quelqu’un de célèbre |
| 5 | Alexis | 27 | 7 | 2006 | G | 1G4 | avoir une bonne note au prochain devoir |
| 6 | Dario | 24 | 5 | 2006 | G | 1G3 | devenir astronaute |
| 7 | Adrien | 14 | 11 | 2006 | G | 1G3 | marcher sur la lune |
| 8 | Yanis | 2 | 6 | 2006 | G | 1G3 | dormir plus longtemps le matin |
| 9 | Sara | 14 | 1 | 2006 | F | 1G3 | aider les autres |
| 10 | Augustin | 24 | 10 | 2006 | G | 1G3 | apprendre à piloter une Formule 1 |
| 11 | Lenny | 24 | 10 | 2006 | G | 1G4 | devenir président |
| 12 | Alan | 2 | 8 | 2006 | G | 1G2 | créer un jeu vidéo |
| 13 | Clement | 18 | 2 | 2005 | G | 1G1 | gérer sa propre entreprise |
| 14 | Nizar | 21 | 12 | 2006 | G | 1G1 | jouer dans le prochain James Bond |
| 15 | Gaellys | 6 | 8 | 2006 | F | 1G1 | monter le tapis rouge à Cannes |
| 16 | Ianis | 29 | 9 | 2006 | G | 1G3 | devenir champion du monde de bras de fer |
| 17 | Martin | 7 | 1 | 2006 | G | 1G1 | devenir youtuber |
| 18 | Timothée | 6 | 10 | 2006 | G | 1G1 | avoir son émission de TV |
| 19 | Mathis | 6 | 12 | 2006 | G | 1G1 | écrire un livre |
| 20 | Bastien | 5 | 1 | 2007 | G | 1G3 | inventer la machine à voyager dans le temps |
| 21 | Yann | 4 | 11 | 2005 | G | 1G2 | devenir une rockstar |
| 22 | Maël | 26 | 2 | 2005 | G | 1G2 | devenir un joueur professionnel de billard |
| 23 | Enzo | 8 | 11 | 2006 | G | 1G3 | ouvrir un restaurant |
| 24 | Alexandre | 27 | 5 | 2006 | G | 1G3 | aller à Poudlard |


# Sélection de colonnes en SQL

**Rappel** : en classe de Première, on a effectué des projections sur les colonnes de la table. L’instruction suivante permet de créer une table `t` contenant les attributs `prenom` et `annee` de la table `eleves`. On dit que l'on fait ainsi une projection sur ces deux colonnes.


```python
>>> t = [{'prénom': e['prénom'], 'année': e['année']} for e in eleves]
>>> t
[{'prénom': 'Sylvain', 'année': 2007},
 {'prénom': 'Romain', 'année': 2006},
 {'prénom': 'Kenzo', 'année': 2006},
 {'prénom': 'Marin', 'année': 2006},
 {'prénom': 'Alexis', 'année': 2006},
 {'prénom': 'Dario', 'année': 2006},
 {'prénom': 'Adrien', 'année': 2006},
 {'prénom': 'Yanis', 'année': 2006},
 {'prénom': 'Sara', 'année': 2006},
 {'prénom': 'Augustin', 'année': 2006},
 {'prénom': 'Lenny', 'année': 2006},
 {'prénom': 'Alan', 'année': 2006},
 {'prénom': 'Clement', 'année': 2005},
 {'prénom': 'Nizar', 'année': 2006},
 {'prénom': 'Gaellys', 'année': 2006},
 {'prénom': 'Ianis', 'année': 2006},
 {'prénom': 'Martin', 'année': 2006},
 {'prénom': 'Timothée', 'année': 2006},
 {'prénom': 'Mathis', 'année': 2006},
 {'prénom': 'Bastien', 'année': 2007},
 {'prénom': 'Yann', 'année': 2005},
 {'prénom': 'Maël', 'année': 2005},
 {'prénom': 'Enzo', 'année': 2006},
 {'prénom': 'Alexandre', 'année': 2006}]
```

En SQL, il est possible de sélectionner certaines colonnes de la table simplement en indiquant après le `SELECT`, les noms des attributs à conserver.

Par exemple, la requête

```sql
SELECT prenom, annee FROM Eleve;
```

permet de ne sélectionner que les attributs `prenom` et `annee` de la table `Eleve` (on fait une projection sur ces deux attributs). Elle produit le résultat :

| prenom    | annee |
|:----------|------:|
| Sylvain   | 2007  |
| Romain    | 2006  |
| Kenzo     | 2006  |
| Marin     | 2006  |
| Alexis    | 2006  |
| Dario     | 2006  |
| Adrien    | 2006  |
| Yanis     | 2006  |
| Sara      | 2006  |
| Augustin  | 2006  |
| Lenny     | 2006  |
| Alan      | 2006  |
| Clement   | 2005  |
| Nizar     | 2006  |
| Gaellys   | 2006  |
| Ianis     | 2006  |
| Martin    | 2006  |
| Timothée  | 2006  |
| Mathis    | 2006  |
| Bastien   | 2007  |
| Yann      | 2005  |
| Maël      | 2005  |
| Enzo      | 2006  |
| Alexandre | 2006  |


**Q2** : Écrivez et testez une requête SQL permettant de sélectionner uniquement les attributs `prenom`, `sexe` et `projet`.

*Réponse* :

<pre><code>



</code></pre>

# Sélectionner de lignes en SQL

**Rappel** : en classe de Première, on a sélectionné des lignes de la table vérifiant une certaine condition. L’instruction suivante permettait de créer une table `nes_en_janvier` contenant les lignes des élèves nés en janvier.

```python
>>> nes_en_janvier = [eleve for eleve in eleves if int(eleve['mois']) == 1] 
>>> nes_en_janvier
[{'prénom': 'Sara',
  'jour': 14,
  'mois': 1,
  'année': 2006,
  'sexe': 'F',
  'classe': '1G3',
  'projet': 'aider les autres'},
 {'prénom': 'Martin',
  'jour': 7,
  'mois': 1,
  'année': 2006,
  'sexe': 'G',
  'classe': '1G1',
  'projet': 'devenir youtuber'},
 {'prénom': 'Bastien',
  'jour': 5,
  'mois': 1,
  'année': 2007,
  'sexe': 'G',
  'classe': '1G3',
  'projet': 'inventer la machine à voyager dans le temps'}]
```


En SQL, il est possible de préciser la condition de sélection en l’écrivant juste après le mot clé `WHERE` comme ci-dessous.

```sql
SELECT * FROM Eleve WHERE mois = 1;
```

| id_eleve | prenom | jour | mois | annee | sexe | classe | projet |
|---:|:---|---:|---:|---:|:---|:---|:---|
| 9 | Sara | 14 | 1 | 2006 | F | 1G3 | aider les autres |
| 17 | Martin | 7 | 1 | 2006 | G | 1G1 | devenir youtuber |
| 20 | Bastien | 5 | 1 | 2007 | G | 1G3 | inventer la machine à voyager dans le temps |

**Q3** : Écrivez et testez une requête SQL permettant d’afficher les élèves nés en 2006.

*Réponse* :

<pre><code>



</code></pre>

**Q4** : Écrivez et testez une requête SQL permettant d’afficher les élèves de 1G3 nés en septembre 2006.

> On peut utiliser les opérateurs `AND` ou `OR` pour combiner les conditions de sélection.

*Réponse* :

<pre><code>



</code></pre>

# Sélectionner des lignes et des colonnes

**Rappel** : L’instruction suivante permettait de créer une table `prenoms_projets_des_filles` contenant les prénoms et les projets des filles de la classe.


```python
>>> prenoms_projets_des_filles = [
        {'prénom': e['prénom'], 'projet': e['projet']} 
        for e in eleves if e['sexe'] == 'F'
    ]
>>> prenoms_projets_des_filles
[{'prénom': 'Sara', 'projet': 'aider les autres'},
 {'prénom': 'Gaellys', 'projet': 'monter le tapis rouge à Cannes'}]
```


En SQL, pour combiner les les sélections sur les lignes et les projections sur les colonnes, il suffit de préciser les attributs de projection après le `SELECT` et préciser la condition de sélection après le `WHERE`.

Par exemple, la requête

```sql
SELECT prenom, projet FROM Eleve WHERE sexe="F";
```

permet d'afficher une table contenant les prénoms et les projets des filles de la classe :

| prenom | projet |
|:---|:---|
| Sara | aider les autres |
| Gaellys | monter le tapis rouge à Cannes |

**Q5** : Écrivez et testez une requête SQL permettant d’afficher les prénoms des élèves nés en août 2006.

*Réponse* :

<pre><code>



</code></pre>

**Q6**: Écrivez et testez une requête SQL permettant d’afficher les id, prénoms et projets des élèves nés les six derniers mois de l’année 2006.

*Réponse* :

<pre><code>



</code></pre>

# Trier les résultats avec ORDER BY

La commande `ORDER BY` permet de trier les résultats d’une requête en précisant juste après sur quel attribut le tri doit se faire.

Par exemple, la requête

```sql
SELECT prenom, annee FROM Eleve ORDER BY prenom ASC;
```

permet de trier le résultat de la requête `SELECT prenom, annee FROM Eleves` selon les prénoms des élèves, ici selon l’ordre alphabétique grâce à `ASC` (« ascendant », du plus petit au plus grand). 

>Il suffirait de remplacer `ASC` par `DESC` (« descendant ») pour que le tri se fasse dans le sens inverse.



**Q7** Écrivez et testez une requête SQL permettant d’afficher les prénoms et l’année de naissance des élèves, le résultat étant trié par ordre croissant d’année de naissance.

*Réponse* :

<pre><code>


</code></pre>

**Q8** : Écrivez et testez une requête SQL permettant d’afficher, du plus jeune au moins jeune, les prénoms des élèves nés en octobre 2006.

> On peut combiner les clauses `WHERE` et `ORDER BY`

*Réponse* :

<pre><code>



</code></pre>

# Quelques fonctions de calculs

Il est possible d'utiliser des fonctions de calculs en SQL. En voici quelques unes.

## Compter le nombre de tuples : `COUNT(*)`

Donnons tout de suite deux exemples.

La requête SQL suivante permet de compter le nombre d’élèves de Première NSI.

```sql
SELECT COUNT(*) AS nbEleves FROM Eleve;
```

| nbEleves |
|---------:|
| 24       |

La requête SQL suivante permet de compter le nombre de garçons de la classe.

```sql
SELECT COUNT(*) AS nbGarcons FROM Eleve WHERE sexe = "G";
```

| nbGarcons |
|----------:|
| 22        |


**Analyse de la deuxième requête** :

* On sélectionne les tuples de la table `Eleve` correspondant à des garçons grâce à l’instruction `FROM Eleves WHERE sexe="G"`
* L’instruction `COUNT(*)` permet de compter *tous* les tuples sélectionnés 
* L’ajout de `AS nbGarcons` permet de nommer le résultat. Cette astuce est optionnelle mais est particulièrement utile pour faciliter la lecture des requêtes.


**Q9** : Écrivez une requête SQL permettant de compter le nombre d’élèves de 1G2.

*Réponse* :

<pre><code>



</code></pre>

**Q10** : Écrivez une requête SQL permettant de compter le nombre d’élèves nés en 2006.

*Réponse* :

<pre><code>



</code></pre>

**Q11** : Écrivez une requête SQL permettant de compter le nombre d’élèves nés en février.

*Réponse* :

<pre><code>



</code></pre>

## Compter le nombre de valeurs distinctes d'un attribut

La requête 

```sql
SELECT annee FROM Eleve;
```

va afficher les années de naissance de chacun des 16 élèves (à savoir 2007, 2006, 2006, …, 2005, ..., 2006, 2007, 2005, ..., 2006)

L’utilisation d’un `DISTINCT` permet d’éviter les redondances dans les résultats affichées. Ainsi, l’instruction

```sql
SELECT DISTINCT annee FROM Eleve;
```

ne va afficher que les années de naissance distinctes des élèves (à savoir 2007, 2006, 2005). N’hésitez pas à tester ces deux requêtes.

Pour compter le nombre d’années de naissance distinctes des élèves de la classe, il suffit d’ajouter un `COUNT` comme suit :

```sql
SELECT COUNT(DISTINCT annee) AS nbAnneesDistinctes FROM Eleve;
```

**Q12** : Écrivez une requête SQL permettant de compter le nombre de mois de naissances différents des élèves de la classe.

*Réponse* :

<pre><code>



</code></pre>

## La fonction `AVG()`

La fonction d'agrégation `AVG()` (de l'anglais, _average_ signifiant "moyenne") dans le langage SQL permet de calculer une valeur moyenne d'un ensemble de valeurs numériques.

Par exemple, la requête

```sql
SELECT AVG(annee) AS anneeMoyenne FROM Eleve;
```

permet de calculer l’année moyenne de naissance des élèves de la classe. 

| anneeMoyenne       |
|--------------------|
| 2005.9583333333333 |


**Q13** : Écrivez une requête SQL permettant de calculer l’âge moyen des élèves de la classe (en 2022). Ce résultat sera nommé `ageMoyen`.

*Réponse* :

<pre><code>



</code></pre>

# Bilan

* Pour interroger une base de données, on doit écrire des requêtes dans le langage SQL via un logiciel de type SGBD.
* Nous avions ici une base de données formée d'une seule table, ce qui n'est pas souvent le cas. Ainsi, vous découvrirez d'autres fonctionnalités du langage SQL dans la suite de ce chapitre où nous travaillerons aussi sur des bases de données formées de plusieurs tables.

---
Germain BECKER, Sébastien Point, Lycée Mounier, ANGERS

Ressource éducative libre distribuée sous [Licence Creative Commons Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International](http://creativecommons.org/licenses/by-nc-sa/4.0/) 

![Licence Creative Commons](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
