Systèmes de gestion de bases de données
=======================================

# Introduction

<img class="centre image-responsive" src="data/database.svg" alt="diagramme" width="150">

Dans une base de données, l'information est stockée dans des fichiers, mais ceux-ci ne sont en général pas lisibles par un humain : ils nécessitent l'utilisation d'un logiciel appelé **système de gestion de bases de données** (abrégé SGBD) pour les exploiter (écrire, lire ou encore modifier les données).

Ce sont de très gros logiciels, fonctionnant pour la grande majorité en mode client/serveur, assez complexes à mettre en œuvre et à utiliser. 

Parmi les SGBD les plus connus, nous avons

- dans le domaine du libre :
    * MariaDB / MySQL
	* PostgreSQL
	* SQLite (qui ne fonctionne pas sur le modèle client/serveur, toute la BDD est stockée dans un fichier qui peut être stocké dans l’arborescence)
- dans le monde propriétaire :
    * Oracle Database
    * IBM DB2
    * Microsoft SQL Server.

Ils sont conçus pour gérer plusieurs millions, voire milliards d'enregistrements de manière fiable et sécurisée. Leur architecture côté serveur est prévue pour être répartie sur plusieurs machines et ainsi permettre une tenue en charge lorsqu'un grand nombre de requêtes parviennent.

# Services rendus

Pour exécuter toutes les tâches évoquées précédemment, les **services rendus** par un SGBD sont les suivants.

## Efficacité de traitement des requêtes

<img class="centre image-responsive" src="data/fast-icon.svg" alt="illustration" width="100">

Les bases de données pouvant être très volumineuses (jusqu'au pétaoctet), il faut que les requêtes soient efficaces (en temps) et les SGBD optimisent la plupart des requêtes SQL que l’on écrit pour que les recherches soient très rapides (même parmi des millions voire milliards d’enregistrement)

## Sécurisation des accès

<img class="centre image-responsive" src="data/security-icon.svg" alt="illustration" width="100">

Les SGBD permettent de gérer les autorisations d'accès à une base de données. Il est en effet souvent nécessaire de contrôler les accès par exemple en permettant à l'utilisateur A de lire et d'écrire dans la base de données alors que l'utilisateur B aura uniquement la possibilité de lire les informations contenues dans cette même base de données.

## Persistance des données

<img class="centre image-responsive" src="data/db-copy.svg" alt="illustration" width="100">

Les fichiers des bases de données sont stockés sur des disques durs dans des ordinateurs, ces ordinateurs peuvent subir des pannes. Il est souvent nécessaire que l'accès aux informations contenues dans une base de données soit maintenu, même en cas de panne matérielle. Les bases de données sont donc dupliquées sur plusieurs ordinateurs afin qu'en cas de panne d'un ordinateur A, un ordinateur B contenant une copie de la base de données présente dans A, puisse prendre le relais. Tout cela est très complexe à gérer : en effet, toute modification de la base de données présente sur l'ordinateur A doit entrainer la même modification de la base de données présente sur l'ordinateur B. Cette synchronisation entre A et B doit se faire le plus rapidement possible, il est fondamental d'avoir des copies parfaitement identiques en permanence. Les SGBD assurent cette persistance des données (duplication pour accès à tout moment et synchronisation des différentes copies de la base de données).

## Gestion des accès concurrents

<img class="centre image-responsive" src="data/data-management-icon.svg" alt="illustration" width="100">

Plusieurs personnes ou applications peuvent avoir besoin d'accéder aux informations contenues dans une base données en même temps. Cela peut parfois poser problème, notamment si les 2 personnes désirent modifier la même donnée au même moment (on parle d'accès concurrent), ce qui arrive très souvent : réservation ou achet en ligne d’un même objet, paiements en ligne à partir ou en direction d’un même compte bancaire, etc. Les SGBD assurent qu’une telle situation ne peut se produire : en particulier ils utilisent un mécanisme de *transaction* (= ensemble d’opérations qui ne peut pas être interrompu par une autre transaction ; cela se faire grâce à l’utilisation de *verrous*, voir cours sur la gestion des processus) qui ne fait passer la BDD d’un état A – antérieur à la transaction – à un état B postérieur, que si plusieurs conditions sont réunies, en particulier qu’aucune opération de la transaction n’a connu d’échecs (dans le cas contraire, toutes les opérations de la transaction sont annulées).

Évidemment, comme nous l’avons déjà vu, s’il s’agit d’un système de gestion de bases de données *relationnelles* (SGBDR), alors le SGBD s’assure aussi en permanence que les **contraintes d’intégrité** soient respectées (contraintes de domaine, de relation et de référence ; voir chapitre sur le modèle relationnel), c’est-à-dire que les données soient constamment cohérentes avec le modèle (mathématique, logique) relationnel de la BDD.

<br>

---
**Références** :

- Cours d’Olivier Lecluse sur les bases de données : [https://www.lecluse.fr/nsi/NSI_T/bdd/intro/](https://www.lecluse.fr/nsi/NSI_T/bdd/intro/)
- Cours de David Roche sur les bases de données : [https://dav74.github.io/site_nsi_term/c1c/](https://dav74.github.io/site_nsi_term/c1c/) 
- Article Wikipédia : [Transactions informatiques](https://fr.wikipedia.org/wiki/Transaction_informatique)
- Les icônes sont issues et adaptées du site [https://uxwing.com/](https://uxwing.com/) et sont sous licence libre (sans attribution), exceptée celle du paragraphe *Gestion des accès concurrents* qui est adaptée d'un travail de [pixellove](https://www.svgrepo.com/svg/506873/db-copy) publié sous licence CC BY sur la plateforme [https://www.svgrepo.com/](https://www.svgrepo.com/). 

---
Germain BECKER, Lycée Mounier, ANGERS

![Licence Creative Commons](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)

